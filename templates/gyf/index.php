<?php

/* ************************************************************************************************************************

G&F

File:			index.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$class = $pageclass->get( 'pageclass_sfx' );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_banner = $this->countModules( 'banner' );
$show_bottom = $this->countModules( 'bottom' );
$show_home = $this->countModules( 'home' );
$show_logo = $this->countModules( 'logo' );
$show_search = $this->countModules( 'search' );
$show_top = $this->countModules( 'top' );
$show_welcome = $this->countModules( 'welcome' );

// Params

?>
<!DOCTYPE html>
<html class="no-js" lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="-va0MHyeEobIVMS7DMRJeFp-2YBv7RlXoLXJAtd8V7U">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>build/bower_components/foundation-sites/dist/css/foundation-flex.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/animate.css/animate.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/fancybox/dist/jquery.fancybox.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/app.css" rel="stylesheet" type="text/css">
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-74928858-1', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php if ( $show_top ) : ?>
		<!-- Begin Top -->
			<section class="top">
				<div class="line"></div>
				<div class="row align-middle">
					<jdoc:include type="modules" name="top" style="xhtml" />
				</div>
			</section>
		<!-- End Top -->
		<?php endif; ?>
		<?php if ( $show_banner ) : ?>
		<!-- Begin Banner -->
			<section class="banner">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="banner" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Banner -->
		<?php endif; ?>
		<?php if ( $show_welcome ) : ?>
		<!-- Begin Welcome -->
			<section class="welcome">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="welcome" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Welcome -->
		<?php endif; ?>
		<?php if ( $show_search ) : ?>
		<!-- Begin Search -->
			<section class="search">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="search" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Search -->
		<?php endif; ?>
		<?php if ( $show_home ) : ?>
		<!-- Begin Home -->
			<section class="home">
				<div class="row">
					<jdoc:include type="modules" name="home" style="xhtml" />
				</div>
			</section>
		<!-- End Home -->
		<?php else : ?>
		<!-- Begin Component -->
			<section class="component">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="component" />
					</div>
				</div>
			</section>
		<!-- End Component -->
		<?php endif; ?>
		<?php if ( $show_logo ) : ?>
		<!-- Begin Logo -->
			<section class="logo">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="logo" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Logo -->
		<?php endif; ?>
		<?php if ( $show_bottom ) : ?>
		<!-- Begin Bottom -->
			<section class="bottom">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="bottom" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Bottom -->
		<?php endif; ?>
		<!-- Begin Copyright -->
			<div class="copyright_wrap">
				<div class="copyright">
					&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
				</div>
			</div>
		<!-- End Copyright -->
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>build/bower_components/jquery/dist/jquery.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/what-input/dist/what-input.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/foundation-sites/dist/js/foundation.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/fancybox/dist/jquery.fancybox.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/wow/dist/wow.min.js"></script>
			<script src="<?php echo $path; ?>build/app.js" type="text/javascript"></script>
		<!-- End Main Scripts -->
	</body>
</html>