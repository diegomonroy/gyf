// JavaScript Document

/* ************************************************************************************************************************

G&F

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

//$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {});

/* JCE */

/* Default */

jQuery( '#submit' ).click(function ( event ) {
	event.preventDefault();
});

/* My Function */

function myFunction() {
	var name = document.getElementById( 'name' ).value;
	var email = document.getElementById( 'email' ).value;
	var telephone = document.getElementById( 'telephone' ).value;
	var subject = document.getElementById( 'subject' ).value;
	var message = document.getElementById( 'e_message' ).value;
	var dataString = 'name1=' + name + '&email1=' + email + '&telephone1=' + telephone + '&subject1=' + subject + '&message1=' + message;
	if ( name === '' || email === '' || telephone === '' || subject === '' || message === '' ) {
		alert( 'Por favor ingrese todos los campos.' );
	} else {
		jQuery.ajax({
			type: 'POST',
			url: 'http://www.inmobiliariaconstructoragyf.com/templates/gyf/build/app.php',
			data: dataString,
			cache: false,
			success: function ( html ) {
				alert( html );
			}
		});
	}
	jQuery( '#jea-contact-form' ).each(function () {
		this.reset();
	});
	return false;
}