// JavaScript Document

/* ************************************************************************************************************************

G&F

File:			Gulpfile.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

// Requires

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var image = require('gulp-imagemin');
var watch = require('gulp-watch');

// CSS

gulp.task('css', function () {
	return gulp
		.src('templates/gyf/assets/css/app.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('templates/gyf/build'));
});

// JS

gulp.task('js', function () {
	return gulp
		.src('templates/gyf/assets/js/app.js')
		.pipe(minifyJS())
		.pipe(rename('app.js'))
		.pipe(gulp.dest('templates/gyf/build'));
});

// Image

gulp.task('image', function () {
	return gulp
		.src('templates/gyf/assets/images/*')
		.pipe(image())
		.pipe(gulp.dest('templates/gyf/build'));
});

// Watch

gulp.task('watch', function () {
	gulp.watch(['templates/gyf/assets/css/**/*.scss'], ['css']);
	gulp.watch(['templates/gyf/assets/js/**/*.js'], ['js']);
});

// Default

gulp.task('default', ['css', 'js', 'image', 'watch']);